"""
Implementation of ConformerCTC model, matching to NeMo, see:
https://github.com/NVIDIA/NeMo/blob/main/nemo/collections/asr/parts/submodules/conformer_modules.py
https://github.com/NVIDIA/NeMo/blob/main/nemo/collections/asr/modules/conformer_encoder.py
https://github.com/NVIDIA/NeMo/blob/main/examples/asr/conf/conformer/conformer_ctc_bpe.yaml
https://github.com/NVIDIA/NeMo/blob/main/nemo/collections/asr/parts/submodules/subsampling.py
https://github.com/NVIDIA/NeMo/blob/c89cacbe26a18568affd297394a45fd294c16807/nemo/collections/asr/modules/conv_asr.py#L403
https://github.com/NVIDIA/NeMo/blob/0073a5e288a3303463c0ca52c6487a98f0f143ea/nemo/collections/asr/parts/submodules/multi_head_attention.py#L127
"""

import math

import tensorflow as tf
from tensorflow.keras import layers as tfl

# ==================================================================================================


class ActivationGlu(tfl.Layer):
    """https://pytorch.org/docs/stable/generated/torch.nn.functional.glu.html"""

    def __init__(self, axis=-1):
        super().__init__()
        self.axis = axis

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ
        a, b = tf.split(x, 2, axis=self.axis)
        b = tf.nn.sigmoid(b)
        x = tf.multiply(a, b)
        return x


# ==================================================================================================


class ConvModule(tfl.Layer):
    def __init__(self, filters: int, kernel_size: int):
        super().__init__()
        self.dropout_rate = 0.1

        self.lnorm = tfl.LayerNormalization(epsilon=0.00001)
        self.convp1 = tfl.Conv1D(filters=filters * 2, kernel_size=1, padding="valid")
        self.glu = ActivationGlu()

        pad = int(math.floor(kernel_size / 2))
        self.pad1d = tf.keras.layers.ZeroPadding1D(padding=(pad, pad))
        self.convd1 = tfl.DepthwiseConv1D(kernel_size=kernel_size, padding="valid")

        # self.bnorm = tfl.BatchNormalization(momentum=0.9)
        self.convp2 = tfl.Conv1D(filters=filters, kernel_size=1, padding="valid")

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ
        residual = x

        # The transpose in NeMo's implementation switches to PyTorch's channel first order

        x = self.lnorm(x, training=training)
        x = self.convp1(x, training=training)
        x = self.glu(x)

        # The pad mask which is here in NeMo's implementation should be already handled by the
        # padded batching in the dataset pipeline

        x = self.pad1d(x)
        x = self.convd1(x, training=training)

        # The batch normalization didn't exist in the onnx checkpoint
        # x = self.bnorm(x, training=training)

        x = tf.keras.activations.swish(x)
        x = self.convp2(x, training=training)

        if training:
            x = tf.nn.dropout(x, rate=self.dropout_rate)

        x = tf.add(x, residual)
        return x


# ==================================================================================================


class RelativePositionalEncoding(tfl.Layer):
    def __init__(self, dimension: int):
        super().__init__()

        self.max_length = 5000
        self.dropout_rate = 0.1
        self.dimension = dimension

        self.create_pe(self.max_length)

    # ==============================================================================================

    def create_pe(self, length):

        positions = tf.range(length - 1, -length, delta=-1, dtype=tf.float32)
        positions = tf.expand_dims(positions, axis=1)

        pos_length = tf.shape(positions)[0]
        self.center_pos = pos_length // 2 + 1

        div_term = tf.range(0, self.dimension, delta=2, dtype=tf.float32)
        div_term = div_term * (-math.log(10000.0) / self.dimension)
        div_term = tf.exp(div_term)

        pe_sin = tf.sin(positions * div_term)
        pe_cos = tf.cos(positions * div_term)

        # Interleave both encodings that every second one is a sinus value and each other a cosine
        pe = tf.stack([pe_sin, pe_cos], axis=-1)
        pe = tf.reshape(pe, [pos_length, self.dimension])
        self.pe = tf.expand_dims(pe, axis=0)

    # ==============================================================================================

    def call(self, x):  # pylint: disable=arguments-differ

        tsteps = tf.shape(x)[1]
        start_pos = self.center_pos - tsteps
        end_pos = self.center_pos + tsteps - 1
        pos_emb = self.pe[:, start_pos:end_pos]

        return pos_emb


# ==================================================================================================


class RelativePositionalMultiHeadAttention(tfl.Layer):
    def __init__(self, dimension: int, num_heads: int):
        super().__init__()

        self.dropout_rate = 0.1
        self.dimension = dimension
        self.num_heads = num_heads
        self.key_dim = int(dimension / num_heads)

        self.linear_q = tfl.Dense(self.dimension)
        self.linear_k = tfl.Dense(self.dimension)
        self.linear_v = tfl.Dense(self.dimension)
        self.linear_pos = tfl.Dense(self.dimension, use_bias=False)
        self.linear_out = tfl.Dense(self.dimension)

        # Two learnable biases
        self.pos_bias_u = tf.Variable(
            initial_value=tf.zeros([self.num_heads, self.key_dim]), trainable=True
        )
        self.pos_bias_v = tf.Variable(
            initial_value=tf.zeros([self.num_heads, self.key_dim]), trainable=True
        )

    # ==============================================================================================

    def call_qkv(self, query, key, value):
        """Transforms query, key and value"""

        nbatch = tf.shape(query)[0]

        q = self.linear_q(query)
        k = self.linear_k(key)
        v = self.linear_v(value)

        q = tf.reshape(q, [nbatch, -1, self.num_heads, self.key_dim])
        k = tf.reshape(k, [nbatch, -1, self.num_heads, self.key_dim])
        v = tf.reshape(v, [nbatch, -1, self.num_heads, self.key_dim])

        q = tf.transpose(q, [0, 2, 1, 3])
        k = tf.transpose(k, [0, 2, 1, 3])
        v = tf.transpose(v, [0, 2, 1, 3])

        return q, k, v

    # ==============================================================================================

    @staticmethod
    def relative_shift(x):

        xshape = tf.shape(x)
        x = tf.pad(x, [[0, 0], [0, 0], [0, 0], [1, 0]])
        x = tf.reshape(x, [xshape[0], xshape[1], xshape[3] + 1, xshape[2]])
        x = x[:, :, 1:]
        x = tf.reshape(x, xshape)

        return x

    # ==============================================================================================

    def call_attention(self, value, scores, training):

        attn = tf.nn.softmax(scores)

        if training:
            attn = tf.nn.dropout(attn, rate=self.dropout_rate)

        x = tf.matmul(attn, value)
        x = tf.transpose(x, [0, 2, 1, 3])

        nbatch = tf.shape(value)[0]
        x = tf.reshape(x, [nbatch, -1, self.num_heads * self.key_dim])

        x = self.linear_out(x)
        return x

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ

        x, p = x
        q, k, v = self.call_qkv(x, x, x)

        # Linear transformation for positional encoding
        nbatch = tf.shape(p)[0]
        p = self.linear_pos(p)
        p = tf.reshape(p, [nbatch, -1, self.num_heads, self.key_dim])
        p = tf.transpose(p, [0, 2, 1, 3])

        q = tf.transpose(q, [0, 2, 1, 3])
        q_with_bias_u = q + tf.cast(self.pos_bias_u, dtype=q.dtype)
        q_with_bias_v = q + tf.cast(self.pos_bias_v, dtype=q.dtype)
        q_with_bias_u = tf.transpose(q_with_bias_u, [0, 2, 1, 3])
        q_with_bias_v = tf.transpose(q_with_bias_v, [0, 2, 1, 3])

        k = tf.transpose(k, [0, 1, 3, 2])
        p = tf.transpose(p, [0, 1, 3, 2])
        matrix_ac = tf.matmul(q_with_bias_u, k)
        matrix_bd = tf.matmul(q_with_bias_v, p)

        matrix_bd = self.relative_shift(matrix_bd)
        # Drops extra elements if there are any left from rel_shift() to match the matrix_ac's size
        matrix_bd = matrix_bd[:, :, :, : tf.shape(matrix_ac)[-1]]

        skd = tf.math.sqrt(tf.constant(self.key_dim, dtype=x.dtype))
        scores = (matrix_ac + matrix_bd) / skd

        x = self.call_attention(v, scores, training)
        return x


# ==================================================================================================


class AttentionModule(tfl.Layer):
    def __init__(self, dimension: int, num_heads: int):
        super().__init__()
        self.dropout_rate = 0.1

        self.lnorm = tfl.LayerNormalization(epsilon=0.00001)
        self.rpmha = RelativePositionalMultiHeadAttention(
            num_heads=num_heads, dimension=dimension
        )

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ
        x, pos_emb = x
        residual = x

        x = self.lnorm(x, training=training)
        x = self.rpmha([x, pos_emb], training=training)

        if training:
            x = tf.nn.dropout(x, rate=self.dropout_rate)

        x = tf.add(x, residual)
        return x


# ==================================================================================================


class FeedForwardModule(tfl.Layer):
    def __init__(self, filters: int):
        super().__init__()

        self.fc_factor = 0.5
        self.dropout_rate = 0.1

        self.norm = tfl.LayerNormalization(epsilon=0.00001)
        self.dense1 = tfl.Dense(filters * 4)
        self.dense2 = tfl.Dense(filters)

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ
        residual = x

        x = self.norm(x)
        x = self.dense1(x, training=training)
        x = tf.keras.activations.swish(x)

        if training:
            x = tf.nn.dropout(x, rate=self.dropout_rate)

        x = self.dense2(x, training=training)
        if training:
            x = tf.nn.dropout(x, rate=self.dropout_rate)

        x = tf.add(self.fc_factor * x, residual)
        return x


# ==================================================================================================


class ConformerBlock(tfl.Layer):
    def __init__(self, dimension: int, kernel_size: int, att_heads: int):
        super().__init__()
        self.dropout_rate = 0.1

        self.ff1 = FeedForwardModule(dimension)
        self.mha = AttentionModule(dimension, att_heads)
        self.conv = ConvModule(dimension, kernel_size)
        self.ff2 = FeedForwardModule(dimension)
        self.norm_out = tfl.LayerNormalization(epsilon=0.00001)

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ
        x, pos_emb = x

        x = self.ff1(x, training=training)
        x = self.mha([x, pos_emb], training=training)
        x = self.conv(x, training=training)
        x = self.ff2(x, training=training)
        x = self.norm_out(x, training=training)

        x = [x, pos_emb]
        return x


# ==================================================================================================


class Encoder(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self, nlayers: int, dimension: int, att_heads: int):
        super().__init__()

        self.dimension = dimension
        self.feature_time_reduction_factor = 4
        self.kernel_size = 31
        self.xscale = math.sqrt(dimension)
        self.dropout_rate = 0.1

        # Subsampling layers
        kernelsize = 3
        self.pad1 = tfl.ZeroPadding2D(padding=1)
        self.conv1 = tfl.Conv2D(
            filters=dimension, kernel_size=kernelsize, strides=2, padding="valid"
        )
        self.conv2 = tfl.Conv2D(
            filters=dimension, kernel_size=kernelsize, strides=2, padding="valid"
        )
        self.linear = tfl.Dense(dimension)

        # Positional encoder
        self.pos_enc = RelativePositionalEncoding(dimension)

        # Conformer Blocks
        self.conf_blocks = []
        for _ in range(nlayers):
            cb = ConformerBlock(
                dimension=dimension, kernel_size=self.kernel_size, att_heads=att_heads
            )
            self.conf_blocks.append(cb)

    # ==============================================================================================

    def get_time_reduction_factor(self):
        return self.feature_time_reduction_factor

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ

        # Subsampling. Reshapes the input to a single channel image with [time, features, 1] and
        # applies the 2D convolutions. Then reshape to [reduced_time, reduced_features * channels]
        # for the dense layer.
        nbatch = tf.shape(x)[0]
        nfeatout = tf.cast(
            x.shape[2] / self.feature_time_reduction_factor, dtype=tf.int32
        )
        x = tf.expand_dims(x, axis=-1)
        x = self.pad1(x)
        x = self.conv1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pad1(x)
        x = self.conv2(x, training=training)
        x = tf.nn.relu(x)
        x = tf.transpose(x, [0, 1, 3, 2])
        x = tf.reshape(x, [nbatch, -1, nfeatout * self.dimension])
        x = self.linear(x)

        # Input encoding scaling
        x = x * self.xscale
        if training:
            x = tf.nn.dropout(x, rate=self.dropout_rate)

        # Positional encoding
        pos_emb = self.pos_enc(x)
        x = [x, pos_emb]

        for conf_block in self.conf_blocks:
            x = conf_block(x, training=training)

        x, pos_emb = x
        return x


# ==================================================================================================


class MyModel(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self, c_input: int, c_output: int, netconfig: dict):
        super().__init__()

        # Check that the netconfig includes all required keys
        req_keys = {"nlayers", "dimension", "attention_heads"}
        if not all((r in netconfig for r in req_keys)):
            raise AttributeError("Required network keys:", req_keys)

        self.n_input = c_input
        self.n_output = c_output
        self.dimension = netconfig["dimension"]

        self.encoder = Encoder(
            netconfig["nlayers"], self.dimension, netconfig["attention_heads"]
        )
        self.feature_time_reduction_factor = self.encoder.get_time_reduction_factor()

        if "extra_deconv" in netconfig and netconfig["extra_deconv"]:
            self.extra_deconv = True
            self.feature_time_reduction_factor /= 2
        else:
            self.extra_deconv = False

        self.model = self.make_model()

    # ==============================================================================================

    def make_model(self):
        input_tensor = tfl.Input(shape=[None, self.n_input], name="input")

        # Used for easier debugging changes
        x = tf.identity(input_tensor)

        x = self.encoder(x)

        if self.extra_deconv:
            x = tfl.Conv1DTranspose(
                filters=int(self.dimension / 2),
                kernel_size=5,
                strides=2,
                padding="same",
                use_bias=True,
            )(x)

        # Map to characters
        x = tfl.Conv1D(filters=self.n_output, kernel_size=1, use_bias=True)(x)

        x = tf.cast(x, dtype=tf.float32)
        x = tf.nn.log_softmax(x)
        output_tensor = tf.identity(x, name="output")

        model = tf.keras.Model(input_tensor, output_tensor, name="ConformerCTC")
        return model

    # ==============================================================================================

    def get_time_reduction_factor(self):
        """Some models reduce the time dimension of the features, for example with striding.
        When the inputs are padded for better batching, it's complicated to get the original length
        from the outputs. So we use this fixed factor."""
        return self.feature_time_reduction_factor

    # ==============================================================================================

    def summary(self, line_length=100, **kwargs):  # pylint: disable=arguments-differ
        print("")
        self.encoder.summary(line_length=line_length, **kwargs)
        print("")
        self.model.summary(line_length=line_length, **kwargs)

    # ==============================================================================================

    @tf.function(experimental_relax_shapes=True)
    def call(self, x, training=False):  # pylint: disable=arguments-differ
        """Call with input shape: [batch_size, steps_a, n_input].
        Outputs a tensor of shape: [batch_size, steps_b, n_output]"""

        x = self.model(x, training=training)
        return x
