FROM nvcr.io/nvidia/tensorflow:22.03-tf2-py3

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
WORKDIR /

RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get install -y --no-install-recommends libsndfile1

# KenLM
RUN apt-get update && apt-get install -y --no-install-recommends libboost-all-dev
RUN git clone --depth 1  https://github.com/kpu/kenlm.git
RUN mkdir -p kenlm/build \
  && cd kenlm/build \
  && cmake .. \
  && make -j $(nproc)

# Solve broken pip "ImportError: No module named pip._internal.cli.main"
RUN python3 -m pip install --upgrade pip

# Dependencies for noise normalization
RUN apt-get update && apt-get install -y --no-install-recommends ffmpeg
RUN pip install --no-cache-dir --upgrade pydub librosa

# Training profiler
RUN pip3 install --upgrade --no-cache-dir tensorboard-plugin-profile

# Dependency to draw graph images
RUN apt-get update && apt-get install -y --no-install-recommends graphviz

# Dependency to show matplot images
RUN apt-get update && apt-get install -y --no-install-recommends python3-tk

# Language model libaries
RUN pip3 install --upgrade --no-cache-dir https://github.com/kpu/kenlm/archive/master.zip
RUN pip3 install --upgrade --no-cache-dir pyctcdecode
RUN pip3 install --upgrade --no-cache-dir install pybind11 && pip3 install --upgrade --no-cache-dir install fastwer

# TfLite runtime
RUN pip3 install --upgrade --no-cache-dir --extra-index-url https://google-coral.github.io/py-repo/ tflite_runtime

# Install corcua
RUN git clone --depth 1 https://gitlab.com/Jaco-Assistant/corcua.git
RUN pip3 install --upgrade --no-cache-dir -e corcua/

# Training package
COPY training/ /Scribosermo/training/
RUN pip3 install --upgrade --no-cache-dir -e /Scribosermo/training/

CMD ["/bin/bash"]
