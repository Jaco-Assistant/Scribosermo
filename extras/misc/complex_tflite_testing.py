import os

import librosa
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tqdm

# import tflite_runtime.interpreter as tflite
from tensorflow import lite as tflite
from tensorflow.keras import layers as tfl
from tensorflow.python.framework.ops import enable_eager_execution

# ==================================================================================================

exportdir = "/tmp/tftests/"
mini_speech_commands_dir = "/tmp/mini_speech_commands/"

# ==================================================================================================


class AudioLayer(tfl.Layer):
    def __init__(self):
        super().__init__()
        audio_sample_rate = 16000
        self.audio_window_samples = int(audio_sample_rate * 0.025)
        self.audio_step_samples = int(audio_sample_rate * 0.01)
        self.n_fft = 512
        num_features = 80

        # Create the matrix here, because building it in the make_model function raised errors
        # when loading the exported model
        self.lin2mel_matrix = librosa.filters.mel(
            sr=audio_sample_rate,
            n_fft=self.n_fft,
            n_mels=num_features,
            fmin=0,
            fmax=audio_sample_rate / 2,
        )

    # ==============================================================================================

    @staticmethod
    def preemphasis(signal, coef=0.97):
        """Emphasizes high-frequency signal components"""

        psig = signal[:, 1:] - coef * signal[:, :-1]
        signal = tf.concat([tf.expand_dims(signal[:, 0], axis=-1), psig], axis=1)
        return signal

    # ==============================================================================================

    @staticmethod
    def per_feature_norm(features):
        """Normalize features per channel/frequency"""
        f_mean = tf.math.reduce_mean(features, axis=1)
        f_mean = tf.expand_dims(f_mean, axis=1)

        # Calculate our own std, because pytorch uses unbiasing,
        # which is approximated by reducing the sample number by one
        f_std = features - f_mean
        f_std = tf.reduce_sum(f_std**2, axis=1)
        n = tf.cast(tf.shape(features)[1], dtype=f_std.dtype) - 1.0
        f_std = tf.sqrt(f_std / n)
        f_std = tf.expand_dims(f_std, axis=1)

        # zero_guard = 1e-5
        zero_guard = 1.0 / 255
        features = (features - f_mean) / (f_std + zero_guard)
        return features

    # ==============================================================================================

    def audio_to_spect(self, audio):
        """Calculate the spectrogram"""

        # Pytorch uses a slightly different spectrogram calculation which is matched to librosa
        # unlike the default tensorflow implementation
        nbatch = tf.shape(audio)[0]

        # Add center padding
        signal = tf.reshape(audio, [nbatch, -1])
        pad_amount = int(self.audio_window_samples // 2)
        signal = tf.pad(signal, [[0, 0], [pad_amount, pad_amount]], "REFLECT")
        signal = tf.reshape(signal, [nbatch, 1, -1])

        # Calculate short-time Fourier transforms with a different windowing approach
        f = tf.signal.frame(
            signal, self.audio_window_samples, self.audio_step_samples, pad_end=False
        )
        w = tf.signal.hann_window(self.audio_window_samples, periodic=False)
        stfts = tf.signal.rfft(f * w, fft_length=[self.n_fft])

        # Obtain the magnitude of the STFT.
        spectrogram = tf.math.real(stfts) ** 2 + tf.math.imag(stfts) ** 2
        spectrogram = tf.squeeze(spectrogram, axis=1)

        return spectrogram

    # ==============================================================================================

    def audio_to_lfbank(self, spectrogram):
        """Calculate log mel filterbanks from spectrogram"""

        lin2mel_matrix = tf.transpose(tf.constant(self.lin2mel_matrix))
        # zero_guard = 2**-24
        zero_guard = 2**-5

        mel_spectrograms = tf.tensordot(spectrogram, lin2mel_matrix, 1)
        mel_spectrograms.set_shape(
            spectrogram.shape[:-1].concatenate(lin2mel_matrix.shape[-1:])
        )
        features = tf.math.log(mel_spectrograms + zero_guard)

        return features

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ

        # Signal augmentations
        audio = self.preemphasis(x, coef=0.97)
        audio = tf.expand_dims(audio, axis=-1)

        # Spectrogram
        spectrogram = self.audio_to_spect(audio)

        # LogFilterbanks
        features = self.audio_to_lfbank(spectrogram)

        # Feature augmentation
        features = self.per_feature_norm(features)

        return features
        # return spectrogram
        # return audio


# ==================================================================================================


class MyModel(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self):
        super().__init__()

        self.model = self.make_model()

    # ==============================================================================================

    def make_model(self):
        input_tensor = tfl.Input(shape=[None], name="input_samples")

        # Used for easier debugging changes
        audio = tf.identity(input_tensor)

        features = AudioLayer()(audio)

        x = features
        output_tensor = tf.identity(x, name="logits")

        model = tf.keras.Model(input_tensor, output_tensor, name="name")
        return model

    # ==============================================================================================

    def summary(self):  # pylint: disable=arguments-differ
        print("")
        self.model.summary(line_length=100)

    # ==============================================================================================

    # This input signature is required that we can export and load the model in ".pb" format
    # with a variable sequence length, instead of using the one of the first input.
    @tf.function(input_signature=[tf.TensorSpec([None, None], tf.float32)])
    def call(self, x):  # pylint: disable=arguments-differ
        """Call with input shape: [1, len_signal], output shape: [1, len_steps, n_alphabet]"""

        x = self.model(x)
        return x


# ==================================================================================================


def download_dataset(data_dir: str):
    if not os.path.exists(data_dir):
        tf.keras.utils.get_file(
            "mini_speech_commands.zip",
            origin="http://storage.googleapis.com/download.tensorflow.org/data/mini_speech_commands.zip",
            extract=True,
            cache_dir=".",
            cache_subdir="/tmp/",
        )


# ==================================================================================================


def get_files(data_dir: str):
    filenames = tf.io.gfile.glob(str(data_dir) + "*/*")
    filenames = tf.random.shuffle(filenames, seed=123)
    filenames = [f for f in filenames.numpy() if f.endswith(b".wav")]
    return filenames


# ==================================================================================================


def load_audio(sample):
    audio_binary = tf.io.read_file(sample["filepath"])
    audio, _ = tf.audio.decode_wav(audio_binary)
    audio = tf.squeeze(audio, axis=-1)
    return audio


# ==================================================================================================


def representative_dataset():

    samples = get_files(mini_speech_commands_dir)
    samples = {"filepath": samples}

    ds = tf.data.Dataset.from_tensor_slices(samples)
    ds = ds.map(map_func=load_audio)
    ds = ds.batch(1)

    # Using a higher number of samples makes the model worse.
    # Might be only a problem when no real model is following afterwards ...
    num_samples = 1
    for data in tqdm.tqdm(
        ds.take(num_samples), total=num_samples, desc="Dataset optimization"
    ):
        yield [tf.dtypes.cast(data, tf.float32)]


# ==================================================================================================


def export_tflite(model, save_path, optimize, fullint):

    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    if optimize:
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
    if fullint:
        converter.representative_dataset = representative_dataset

    tflite_model = converter.convert()

    with open(save_path, "wb+") as file:
        file.write(tflite_model)


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def plot_spectrogram(spectrogram, ax):
    if len(spectrogram.shape) > 2:
        assert len(spectrogram.shape) == 3
        spectrogram = np.squeeze(spectrogram, axis=-1)
    # Convert the frequencies to log scale and transpose, so that the time is
    # represented on the x-axis (columns).
    # Add an epsilon to avoid taking a log of zero.
    if np.amin(spectrogram) >= 0:
        log_spec = np.log(spectrogram.T + np.finfo(float).eps)
    else:
        # Using features instead of spectrogram
        log_spec = spectrogram.T
    height = log_spec.shape[0]
    width = log_spec.shape[1]
    X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
    Y = range(height)
    ax.pcolormesh(X, Y, log_spec)


def plot_values(waveform, features, title: str):
    fig, axes = plt.subplots(2, figsize=(8, 6))
    fig.suptitle(title, fontsize=20)
    timescale = np.arange(waveform.shape[0])
    axes[0].plot(timescale, waveform.numpy())
    axes[0].set_title("Waveform")
    axes[0].set_xlim([0, 16000])

    plot_spectrogram(features, axes[1])
    axes[1].set_title("Features")


# ==================================================================================================


def main():
    if not os.path.isdir(exportdir):
        os.makedirs(exportdir)

    # Hide gpu to speed up exporting
    os.environ["CUDA_VISIBLE_DEVICES"] = ""

    # Eager execution is required for pb and tflite exports
    enable_eager_execution()

    # Download dataset if not existing
    download_dataset(mini_speech_commands_dir)

    model_tl = MyModel()
    model_tl.build(input_shape=(None, None))
    model_tl.summary()

    print("\nExporting models ...")
    export_tflite(
        model_tl, exportdir + "model_float32.tflite", optimize=False, fullint=False
    )
    export_tflite(
        model_tl, exportdir + "model_float16.tflite", optimize=True, fullint=False
    )
    export_tflite(
        model_tl, exportdir + "model_int8.tflite", optimize=True, fullint=True
    )

    inputdata = get_files(mini_speech_commands_dir)[-1]
    print("Loading file:", inputdata)
    inputdata = load_audio({"filepath": inputdata})
    inputdata = tf.expand_dims(inputdata, axis=0)
    print("Input:", inputdata)

    print("\nmodel_float32:")
    interpreter = tflite.Interpreter(model_path=exportdir + "model_float32.tflite")
    print("Input details:", interpreter.get_input_details())
    prediction = predict(interpreter, inputdata)
    print("Prediction:", prediction)
    print(
        "Min-Max-Mean:",
        tf.reduce_min(prediction),
        tf.reduce_max(prediction),
        tf.reduce_mean(prediction),
    )
    plot_values(inputdata[0], prediction[0], "model_float32")

    print("\nmodel_float16:")
    interpreter = tflite.Interpreter(model_path=exportdir + "model_float16.tflite")
    print("Input details:", interpreter.get_input_details())
    prediction = predict(interpreter, inputdata)
    print("Prediction:", prediction)
    print(
        "Min-Max-Mean:",
        tf.reduce_min(prediction),
        tf.reduce_max(prediction),
        tf.reduce_mean(prediction),
    )
    plot_values(inputdata[0], prediction[0], "model_float16")

    print("\nmodel_int8:")
    interpreter = tflite.Interpreter(model_path=exportdir + "model_int8.tflite")
    print("Input details:", interpreter.get_input_details())
    prediction = predict(interpreter, inputdata)
    print("Prediction:", prediction)
    print(
        "Min-Max-Mean:",
        tf.reduce_min(prediction),
        tf.reduce_max(prediction),
        tf.reduce_mean(prediction),
    )
    plot_values(inputdata[0], prediction[0], "model_int8")

    # Show all plots at once
    plt.show()


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
