# Crossbuild `ds_ctcdecoder` for `aarch64`

Target platform:

- Raspberry Pi 4 Model B (Cortex-A72)
- Ubuntu Server 20.04 64 bit with Python 3.8

## Build DeepSpeech's building container

```
git clone https://github.com/mozilla/DeepSpeech.git
cd DeepSpeech/
make Dockerfile.build
cd ..
docker build -t dsbuild - < DeepSpeech/Dockerfile.build
```

## Enable container cross-building

Setup [`qemu-user-static`](https://github.com/multiarch/qemu-user-static) on your system.

## Build `swig`

From now on the working directory is `Scribosermo/extras/misc/ctcdecoder_aarch64` (where this README is located).

```
docker build -t xbuild_swig -f $PWD/Dockerfile_crossbuild_swig .
```

Extract `ds-swig.tar.gz`:

```
docker run --rm -it -v $PWD:/host_dir xbuild_swig cp /ds-swig.tar.gz /host_dir
```

## Build `ds_ctcdecoder`

```
docker build -t xbuild_decoder -f $PWD/Dockerfile_crossbuild_decoder .
```

Extract the Python wheel:

```
docker run --rm -it -v $PWD:/host_dir xbuild_decoder bash -c 'cp /DeepSpeech/native_client/ctcdecode/dist/* /host_dir'
```
