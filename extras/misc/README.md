# Various scripts

Collection of various scripts that have no special topics.

<br>

### Testing transcription on RaspberryPi

Build container:

```bash
docker build -t scribosermo_arm64 - < Scribosermo/extras/misc/Containerfile_arm64
```

Run script: \
(For performance tests, restart the pi every time, to prevent caching speedups) \
(You might need to update the testing script to use smaller random audio signals, because the Raspi hasn't enough memory)

```bash
docker run --privileged --rm --network host -it \
  --volume "$(pwd)"/Scribosermo/exporting/:/Scribosermo/exporting/:ro \
  --volume "$(pwd)"/checkpoints/:/checkpoints/:ro \
  scribosermo_arm64

python3 /Scribosermo/exporting/testing_speed.py
```

<br>

### Building optimized tensorflow-lite runtime

Follow instructions of: https://github.com/PINTO0309/TensorflowLite-bin/issues/17#issuecomment-1085231340
The prebuild wheel can then be found at: `./tensorflow/lite/tools/pip_package/gen/tflite_pip/python3.8/dist/`

<br>

### Dataset syntax conversion

Convert from _DeepSpeech_ format to _corcua_ format like this:

```bash
python3 /Scribosermo/extras/misc/convert_ds2cc.py --source_path "/data_prepared/de/tuda/" \
  --target_path "/data_prepared/de/tuda2/" --train "train_s.csv" --dev "dev_s.csv" --test "test_s.csv" --remove_text_commas

# Remove "/DeepSpeech" path prefix (old directory structure)
sed 's/\/DeepSpeech//g' train.csv > train2.csv
```
