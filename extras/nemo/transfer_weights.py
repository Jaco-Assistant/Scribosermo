import itertools
import json
import os

import numpy as np
import onnx
import tensorflow as tf
from onnx import TensorProto, helper
from onnx.numpy_helper import to_array as onnx_to_array
from onnx_tf.backend import prepare

from scode import nets, pipeline, utils

# ==================================================================================================

tf.config.run_functions_eagerly(True)

netconfig = {
    "name": "conformerctc",
    "nlayers": 18,
    "dimension": 512,
    "attention_heads": 8,
}
c_input = 80
c_output = 129
onnx_path = "/dsp_nemo/models/stt_en_conformer_ctc_large.onnx"
test_csv = "/Scribosermo/extras/nemo/data/test.csv"
save_path = "/checkpoints/en/conformerctc-large/"

pl_config = {
    "alphabet_path": "/Scribosermo/data/en/alphabet-sentencepiece.json",
    "audio_features": {
        "audio_sample_rate": 16000,
        "num_features": 80,
        "window_len": 0.025,
        "window_step": 0.01,
    },
    "training": {
        "sort_datasets": False,
    },
    "augmentations": {
        "signal": {
            "dither": {"use_train": True, "use_test": False, "factor": 1e-05},
            "preemphasis": {"use_train": True, "use_test": True, "coefficient": 0.97},
        },
        "spectrogram": {},
        "features": {"normalize_features": {"use_train": True, "use_test": True}},
    },
}

# ==================================================================================================


def create_model():

    network_type = netconfig["name"]
    mynet = getattr(nets, network_type)
    model = mynet.MyModel(c_input=c_input, c_output=c_output, netconfig=netconfig)

    model.build(input_shape=(None, None, c_input))
    model.summary()

    flops = utils.get_flops(model, ntime=458, nfeat=c_input) / 1e9
    print("The model has {:.2f}G multiply-add operations".format(flops))

    return model


# ==================================================================================================


def test_build_random_input():

    model = create_model()
    inp = tf.random.normal([1, 458, c_input])
    out = model(inp)
    print("Input:", tf.shape(inp))
    print("Output:", tf.shape(out))


# ==================================================================================================


def transfer_onnx_weights():

    # Create tensorflow model
    # Make sure that all layers are created in the same order (in the init method) as they are
    # called, else the sorting won't match the one of the onnx model
    model = create_model()
    # ws = [w.shape for w in model.get_weights()]
    # print("\n", len(ws), ws)

    # Load onnx model
    onnx_model = onnx.load(onnx_path)

    # Weights are not sorted, so we need to extract the sorting from the layers
    nodes = [t for t in onnx_model.graph.node]
    nodes = [n.input for n in nodes]

    # Drop layers without weights
    nodes = [n for n in nodes if len(n) > 1]
    # print("\n", len(nodes), nodes)

    # Extract weights and their ids
    weights = [t for t in onnx_model.graph.initializer]
    weights = [(w.name, onnx_to_array(w)) for w in weights]
    # ws = [(n, w.shape) for n, w in weights]
    # print("\n", len(ws), ws)

    # Drop weights with a single, two or four weights, they are from the padding operations
    weights = [w for w in weights if w[1].shape != (1,)]
    weights = [w for w in weights if w[1].shape != (2,)]
    weights = [w for w in weights if w[1].shape != (4,)]
    # ws = [(n, w.shape) for n, w in weights]
    # print("\n", len(ws), ws)

    # Sort weights
    nodes = list(itertools.chain.from_iterable(nodes))
    # print("\n", len(nodes), nodes)
    weights = sorted(weights, key=lambda item: nodes.index(item[0]))
    # ws = [(n, w.shape) for n, w in weights]
    # print("\n", len(ws), ws)

    # Move the positional biases from the attention layers before the rest of this layer's weights
    new_order = []
    buffer = []
    previous = ()
    for n, w in weights:
        if "self_attn" not in n:
            if previous != ():
                new_order.append(previous)
            previous = (n, w)
        else:
            if previous != ():
                buffer.append(previous)
                previous = ()
            if "linear_out" in n:
                new_order.extend(buffer)
                buffer = []
                new_order.append((n, w))
            elif "pos_bias" in n:
                new_order.append((n, w))
            else:
                buffer.append((n, w))
    if previous != ():
        new_order.append(previous)
    weights = new_order

    # Transpose weights
    t_weights = []
    for n, w in weights:
        if len(w.shape) == 1:
            tw = w
        elif len(w.shape) == 2:
            tw = w
        elif len(w.shape) == 4:
            tw = np.transpose(w, (2, 3, 1, 0))
        elif w.shape[1] == 1:
            tw = np.transpose(w, (2, 0, 1))
        else:
            tw = np.transpose(w, (2, 1, 0))
        t_weights.append([n, tw])
    # ws = [(n, w.shape) for n, w in t_weights]
    # print("\n", len(ws), ws)

    # Check that all weights' shapes are existing
    wsm = [w.shape for w in model.get_weights()]
    for i, (n, wo) in enumerate(t_weights):
        if wsm[i] != wo.shape:
            i1 = max(0, i - 3)
            i2 = min(i + 3, len(t_weights) - 1)
            print("\n", "Missing:", i, wsm[i], n, wo.shape)
            print("\n", wsm[i1:i2])
            print("\n", [(w.shape) for m, w in t_weights][i1:i2])
            print("\n", [(m, w.shape) for m, w in t_weights][i1:i2])
            exit()

    # Finally copy the weights into our tensorflow model
    t_weights = [w for _, w in t_weights]
    model.set_weights(t_weights)

    print("Copied model weights")
    return model


# ==================================================================================================


def compare_outputs():
    np.set_printoptions(edgeitems=10)

    tds = pipeline.create_pipeline(test_csv, 1, pl_config, mode="test")
    for samples in tds:
        inp = samples["features"]
        length = tf.shape(samples["features"])[1]
        length = tf.expand_dims(length, axis=0).numpy()

    # np.random.seed(123)
    # inp = np.random.normal(size=[1, 458, c_input]).astype(np.float32)
    # length = [458]
    # print("Input: ", inp)

    model = transfer_onnx_weights()
    out_tf = model(inp)

    onnx_model = onnx.load(onnx_path)
    onnxtf_model = prepare(onnx_model)
    ort_inputs = {
        "audio_signal": np.transpose(inp, [0, 2, 1]),
        "length": np.array(length),
    }
    out_ox = onnxtf_model.run(ort_inputs)
    out_ox = out_ox.logprobs

    print("Result TF:", out_tf)
    print("Result ONNX:", out_ox)
    print("Shape ONNX:", out_ox.shape)

    print("")
    msg = "Are Close {}: {}/{}"
    countclose = lambda a, b, t: np.sum(np.isclose(a, b, rtol=t, atol=t))
    print(msg.format("(1e-6)", countclose(out_tf, out_ox, 0.000001), out_ox.size))
    print(msg.format("(1e-5)", countclose(out_tf, out_ox, 0.00001), out_ox.size))
    print(msg.format("(1e-4)", countclose(out_tf, out_ox, 0.0001), out_ox.size))
    print(msg.format("(1e-3)", countclose(out_tf, out_ox, 0.001), out_ox.size))
    print(msg.format("(1e-2)", countclose(out_tf, out_ox, 0.01), out_ox.size))
    print(msg.format("(1e-1)", countclose(out_tf, out_ox, 0.1), out_ox.size))

    print("")
    print("Greedy TF:", tf.argmax(out_tf, axis=-1))
    print("Greedy ONNX:", tf.argmax(out_ox, axis=-1))
    print("")


# ==================================================================================================


def debug_models():
    """Compare outputs layer by layer. Partly taken from:
    https://github.com/onnx/onnx-tensorflow/blob/master/example/test_model_large_stepping.py"""

    np.set_printoptions(edgeitems=10)

    # Get onnx layer names from netron website
    # log_layer = "672"
    # log_layer = "674"
    # log_layer = "687"
    # log_layer = "690"  # Subsample-Dense
    # log_layer = "692"
    # log_layer = "772"  # FF1-LN  <- small variance from LayerNorm ops
    # log_layer = "777"
    # log_layer = "783"  # FF1-ADD  <- different after 6th digit
    # log_layer = "711"  # PosEmb  <- different after 4-5th digit
    # log_layer = "850"  # MHA1-q_with_bias_u  <- different after 3-7th digit
    # log_layer = "854"  # MHA1-matrix_ac  <- different after 4-5th digit
    # log_layer = "839"  # MHA1-linear_p
    # log_layer = "855"  # MHA1-p-transposed
    # log_layer = "926"  # MHA1-score  <- different after 4-5th digit
    # log_layer = "950"  # MHA1-ADD
    # log_layer = "978"  # Conv1-ADD
    # log_layer = "1011"  # CB1-LN  <- different after 5-6th digit
    log_layer = "4761"  # CB16-LN  <- different after 4-5th digit

    # Extend output of onnx model with given layer
    onnx_model = onnx.load(onnx_path)
    more_outputs = []
    output_to_check = []
    for node in onnx_model.graph.node:
        if node.output[0] == log_layer:
            more_outputs.append(
                helper.make_tensor_value_info(
                    node.output[0], TensorProto.FLOAT, (100, 100)
                )
            )
            output_to_check.append(node.output[0])
    onnx_model.graph.output.extend(more_outputs)
    onnxtf_model = prepare(onnx_model)

    # Build tf model
    # To get the value at specific layers just add a print statement into the model's code
    tfmodel = transfer_onnx_weights()

    # Prepare example file
    tds = pipeline.create_pipeline(test_csv, 1, pl_config, mode="test")
    for samples in tds:
        inp = samples["features"]
        length = tf.shape(samples["features"])[1]
        length = tf.expand_dims(length, axis=0).numpy()

    _ = tfmodel(inp)

    ort_inputs = {
        "audio_signal": np.transpose(inp, [0, 2, 1]),
        "length": np.array(length),
    }
    out_ox = onnxtf_model.run(ort_inputs)
    out_ox = out_ox[log_layer]

    # Depending on the layer, the shape order might change
    out_ox = tf.transpose(out_ox, [0, 1, 2])
    # out_ox = tf.transpose(out_ox, [0, 1, 2, 3])
    print("Output ONNX:", tf.convert_to_tensor(out_ox))


# ==================================================================================================


def save_model():

    model = transfer_onnx_weights()
    # model.build(input_shape=(None, None, c_input))
    # model.summary()

    inp = tf.random.normal([1, 458, c_input])
    _ = model(inp)

    # Export the model
    if os.path.exists(save_path):
        utils.delete_dir(save_path)
    os.makedirs(save_path)
    tf.keras.models.save_model(model, save_path, include_optimizer=False)

    # Export current config next to the checkpoints
    config = utils.get_config()
    config["network"] = netconfig
    path = os.path.join(save_path, "config_export.json")
    with open(path, "w+", encoding="utf-8") as file:
        json.dump(config, file, indent=2)

    print("Saved model")


# ==================================================================================================

if __name__ == "__main__":
    # test_build_random_input()
    # transfer_onnx_weights()
    compare_outputs()
    # debug_models()
    save_model()
