import argparse
import os

import pandas as pd

# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Split dataset into parts")
    parser.add_argument("csv_to_split_path", type=str)
    parser.add_argument("--file_appendix", type=str, default="")
    parser.add_argument(
        "--split",
        type=str,
        default="",
        help="Split into parts. Argument usage: --split '70|15|15'",
    )
    args = parser.parse_args()

    # Keep the german 0 as "null" string
    data = pd.read_csv(args.csv_to_split_path, keep_default_na=False)

    splits = [int(s) for s in args.split.split("|")]
    if len(splits) != 3 or sum(splits) != 100:
        raise ValueError

    splits = [s * 0.01 for s in splits]
    datasize = len(data)

    data_train = data.iloc[: int(datasize * splits[0])]
    data_dev = data.iloc[
        int(datasize * splits[0]) : int(datasize * splits[0] + datasize * splits[1])
    ]
    data_test = data.iloc[int(datasize * splits[0] + datasize * splits[1]) :]

    msg = "Length of train, dev and test files: {} {} {}"
    print(msg.format(len(data_train), len(data_dev), len(data_test)))

    outpath = os.path.dirname(args.csv_to_split_path)
    data_train.to_csv(
        os.path.join(outpath, "train" + args.file_appendix + ".csv"),
        index=False,
        encoding="utf-8",
    )
    data_dev.to_csv(
        os.path.join(outpath, "dev" + args.file_appendix + ".csv"),
        index=False,
        encoding="utf-8",
    )
    data_test.to_csv(
        os.path.join(outpath, "test" + args.file_appendix + ".csv"),
        index=False,
        encoding="utf-8",
    )


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("FINISHED")
